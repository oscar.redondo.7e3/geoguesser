package com.example.geoguesser;

public class Preguntas {

    private int respuesta;
    private int imgagen;
    private String pista;
    private String op1,op2,op3,op4;

    public String getPista() {
        return pista;
    }

    public void setPista(String pista) {
        this.pista = pista;
    }

    public Preguntas(int imgagen, int respuesta, String pista, String op1, String op2, String op3, String op4) {
        this.respuesta = respuesta;
        this.imgagen = imgagen;
        this.pista = pista;
        this.op1 = op1;
        this.op2 = op2;
        this.op3 = op3;
        this.op4 = op4;
    }

    public int getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(int respuesta) {
        this.respuesta = respuesta;
    }

    public int getImgagen() {
        return imgagen;
    }

    public void setImgagen(int imgagen) {
        this.imgagen = imgagen;
    }

    public String getOp1() {
        return op1;
    }

    public void setOp1(String op1) {
        this.op1 = op1;
    }

    public String getOp2() {
        return op2;
    }

    public void setOp2(String op2) {
        this.op2 = op2;
    }

    public String getOp3() {
        return op3;
    }

    public void setOp3(String op3) {
        this.op3 = op3;
    }

    public String getOp4() {
        return op4;
    }
}
