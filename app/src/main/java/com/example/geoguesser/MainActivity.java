package com.example.geoguesser;

import androidx.annotation.ColorRes;
import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity implements  View.OnClickListener {

    TextView textViewPregunta ;
    ImageView imageViewPregunta;
    ProgressBar barradeProgreso;
    TextView textViewPuntuacion;
    TextView textViewProgreso;
    Button bOpcion1,bOpcion2,bOpcion3,bOpcion4,bSkip;
    Preguntas [] provincias;
    Integer [] orden= {0,1,2,3,4,5,6,7,8,9};
    int currentPregunta;
    int  currentSkips;
    final int  totalSkips=3;
    int ProgresBar=0;
    private Handler mHandle = new Handler();
    double puntuacion=0.0;
    double acierto=1.0;
    double  fallo =0.5;
    AlertDialog.Builder dialog;
    ViewModel  myModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //he intentado harcelo con  view model pero  me da error tod o el rato
       // myModel= new ViewModelProvider(this).get(MainActivity.class);


        //asigno todos los elementos
        textViewPuntuacion= findViewById(R.id.TextViewPuntuacion);
        textViewPregunta= findViewById(R.id.TextViewPregunta);
        imageViewPregunta = findViewById(R.id.ImagePregunta);
        barradeProgreso = findViewById(R.id.ProgresBarTime);
        textViewProgreso = findViewById(R.id.TextViewIndicadorDeProgreso);
        bOpcion1 = findViewById(R.id.ButtonOpcion1);
        bOpcion1.setOnClickListener(this);
        bOpcion2 = findViewById(R.id.ButtonOpcion2);
        bOpcion2.setOnClickListener(this);
        bOpcion3 = findViewById(R.id.ButtonOpcion3);
        bOpcion3.setOnClickListener(this);
        bOpcion4 = findViewById(R.id.ButtonOpcion4);
        bOpcion4.setOnClickListener(this);
        bSkip = findViewById(R.id.ButtonSkipp);
        bSkip.setOnClickListener(this);
        dialog = new AlertDialog.Builder(this);
        //creo las preguntas
        provincias= new Preguntas[]{new Preguntas(R.drawable.cadiz, 2, "Andalucía", "Álava", "Cadiz", "Jaén", "Asturias"),
                new Preguntas(R.drawable.barcelona, 1, "Cataluña", "Barcelona", "Alicante", "Murcia", "Girona"),
            new Preguntas(R.drawable.granada, 3, "Andalucía", "Alicante", "Barcelona", "Granada", "Soria"),
            new Preguntas(R.drawable.tarragona, 2, "Cataluña", "Álava", "Tarragona", "Lerida", "Alicante"),
            new Preguntas(R.drawable.madrid, 1, "Comunidad de Madrid", "Madrid", "Barcelona", "Sevilla", "Badajoz"),
            new Preguntas(R.drawable.asturias, 4, "Principado de Asturias", "Álava", "Cantabria", "Vizcaya", "Asturias"),
            new Preguntas(R.drawable.zaragoza, 2, "Aragon", "Álava", "Zaragoza", "Teruel", "Madrid"),
            new Preguntas(R.drawable.segovia, 2, "Castilla y León", "Álava", "Segovia", "Jaén", "León"),
            new Preguntas(R.drawable.sevilla, 1, "Andalucía", "Sevilla", "Barcelona", "Jaén", "Asturias"),
            new Preguntas(R.drawable.toledo, 3, "Castilla la mancha","Ciudad  Real", "Barcelona", "Toledo", "Asturias")};

        //asi  desordeno una  lista para que sea alatorio todo el rato
        List<Integer> listaOrdenada = Arrays.asList(orden);
        Collections.shuffle(listaOrdenada);

        siguientePregunta();
        }





        //utilizo el onclick con u siwtch para cada boton
    @Override
    public void onClick(View v){

        Button b = (Button) v;
        int opcion = b.getId();

        switch (opcion){

            case R.id.ButtonOpcion1:
                bSkip.setEnabled(false);
                habilitarDeshabilitarBotonoes(false);
                comprovarSiEsCorrecto(1);
                break;
            case R.id.ButtonOpcion2:
                bSkip.setEnabled(false);
                habilitarDeshabilitarBotonoes(false);
                comprovarSiEsCorrecto(2);
                break;
            case R.id.ButtonOpcion3:
                bSkip.setEnabled(false);
                habilitarDeshabilitarBotonoes(false);
                comprovarSiEsCorrecto(3);
                break;
            case R.id.ButtonOpcion4:
                bSkip.setEnabled(false);
                habilitarDeshabilitarBotonoes(false);
                comprovarSiEsCorrecto(4);
                break;
            case R.id.ButtonSkipp:
                bSkip.setEnabled(false);
                currentSkips++;
                mostrarSkip();
                acierto=0.3;
                break;
        }

    }


    //actualiza el numero de pistas que te quedan y lo deshabilita cuando no hay mas
    public void mostrarSkip(){
        bSkip.setText(provincias[orden[currentPregunta]].getPista());
        if(currentSkips==3){
            bSkip.setEnabled(false);
            bSkip.setBackgroundColor(getResources().getColor(R.color.dissable));
        }

    }

    //carga los textos de una pregunta nueva  y reseta la barra de progreso
    public void siguientePregunta(){
        //habilito los botones
        habilitarDeshabilitarBotonoes(true);


        ProgresBar=0;
        ProgresBarSetuP(currentPregunta);
       textViewPuntuacion.setText("Puntuación: "+ puntuacion);
        bOpcion1.setBackgroundColor(getResources().getColor(R.color.botonDef));
        bOpcion2.setBackgroundColor(getResources().getColor(R.color.botonDef));
        bOpcion3.setBackgroundColor(getResources().getColor(R.color.botonDef));
        bOpcion4.setBackgroundColor(getResources().getColor(R.color.botonDef));
        imageViewPregunta.setImageResource(provincias[orden[currentPregunta]].getImgagen());
        bOpcion1.setText(provincias[orden[currentPregunta]].getOp1());
        bOpcion2.setText(provincias[orden[currentPregunta]].getOp2());
        bOpcion3.setText(provincias[orden[currentPregunta]].getOp3());
        bOpcion4.setText(provincias[orden[currentPregunta]].getOp4());
        bSkip.setText(getString(R.string.skipOpen)+(totalSkips-currentSkips+"/"+totalSkips+")"));
        textViewProgreso.setText("Pregunta "+ (currentPregunta+1)+" de 10");

       if (currentSkips<3)bSkip.setEnabled(true);

        //reseteo el valor de acierto por si en la anterior ha habiado una pista
        acierto=1.0;


    }

    //pone a verde la respuesta correscta
    public void printarResuestaCorrecta(int respuesta){
        switch (respuesta){

            case 1:
                bOpcion1.setBackgroundColor(getResources().getColor(R.color.acierto));
                break;
            case 2:
                bOpcion2.setBackgroundColor(getResources().getColor(R.color.acierto));
                break;
            case 3:
                bOpcion3.setBackgroundColor(getResources().getColor(R.color.acierto));
                break;
            case 4:
                bOpcion4.setBackgroundColor(getResources().getColor(R.color.acierto));
                break;
        }

    }
    //pinta verde la respuesta correcta y rojo la incorrecta
    private void printarRespuestaIncorrecta(int respuesta) {
        switch (respuesta){

            case 1:
                bOpcion1.setBackgroundColor(getResources().getColor(R.color.error));
                break;
            case 2:
                bOpcion2.setBackgroundColor(getResources().getColor(R.color.error));
                break;
            case 3:
                bOpcion3.setBackgroundColor(getResources().getColor(R.color.error));
                break;
            case 4:
                bOpcion4.setBackgroundColor(getResources().getColor(R.color.error));
                break;
        }
        switch (provincias[orden[currentPregunta]].getRespuesta()){

            case 1:
                bOpcion1.setBackgroundColor(getResources().getColor(R.color.acierto));
                break;
            case 2:
                bOpcion2.setBackgroundColor(getResources().getColor(R.color.acierto));
                break;
            case 3:
                bOpcion3.setBackgroundColor(getResources().getColor(R.color.acierto));
                break;
            case 4:
                bOpcion4.setBackgroundColor(getResources().getColor(R.color.acierto));
                break;
        }

    }

    //comprueba si la respuesta es correcta,actualiza la puntuacion y el numero de pregunta y si llevamos mas de 10 preguntas abre el dialogo
    public void comprovarSiEsCorrecto(int respuesta){


        if (respuesta==provincias[orden[currentPregunta]].getRespuesta()){
            printarResuestaCorrecta(respuesta);
            currentPregunta++;
            puntuacion=puntuacion+acierto;

        }else {

            printarRespuestaIncorrecta(respuesta);
            currentPregunta++;
            puntuacion=puntuacion-fallo;
        }

        if (currentPregunta<10){
            //hace 5 segundos de espera para que el usuario vea el resultaod de la pregunta
            Handler handlerEspera =new Handler();
            handlerEspera.postDelayed(new Runnable() {
                @Override
                public void run() {
                    siguientePregunta();
                }
            },5000);

        }
        else {
            dialog();
        }


    }
    //clase polimorfica que deshabilita  o habilita los bonoes los  botones para que hata que no salga la siguiente pregunta el usuario no pueda seguir haciendo click
    public void habilitarDeshabilitarBotonoes(Boolean hab){
        bOpcion1.setEnabled(hab);
        bOpcion2.setEnabled(hab);
        bOpcion3.setEnabled(hab);
        bOpcion4.setEnabled(hab);
    }




    //barra de progreso que es una cuenta attras con currentPregunta!=pregunta  evito que el contador se sume si ya hemos pasado la pregunta
    public void ProgresBarSetuP(final int pregunta){
        new Thread(new Runnable() {

            @Override
            public void run() {
                while (ProgresBar<100) {
                    if (currentPregunta!=pregunta) ProgresBar=100;
                    ProgresBar++;
                    SystemClock.sleep(500);
                    mHandle.post(new Runnable() {
                        @Override
                        public void run() {
                            barradeProgreso.setProgress(ProgresBar);
                        }
                    });
                }
                    mHandle.post(new Runnable() {
                        @Override
                        public void run() {
                            if (currentPregunta==pregunta) {
                                comprovarSiEsCorrecto(0);

                            }


                        }
                    });

            }
        }).start();

    }


    //crea el dialigo para reiniciar o salir

    public void dialog(){
        int res;
        res=(int)puntuacion*10;
        String titulo="Has sacado un  "+res+" de 100";


        dialog.setTitle(titulo);
        dialog.setMessage("Quieres volver a  intentarlo?");
        dialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                MainActivity.this.finish();
            }
        });
        dialog.setPositiveButton("Si", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                Intent  intent = getIntent();
                finish();
                startActivity(intent);
                dialog.cancel();
            }
        });
        dialog.show();


    }

    //para salver el estado
    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        //primero tienes que añadir al manifest algo y luego pasas tidas las variables y funciones al QuizView Model nueva clase con extens al main  y que en el main solo queden las que escriben algo en pantalla
        outState.putDouble("puntuacion",puntuacion);
        outState.putString("puntuaciones",textViewPuntuacion.getText().toString());
        outState.putString("progreso",textViewProgreso.getText().toString());
        outState.putInt("pistas",currentSkips);
        outState.putString("b1",bOpcion1.getText().toString());
        outState.putString("b2",bOpcion2.getText().toString());
        outState.putString("b3",bOpcion3.getText().toString());
        outState.putString("b4",bOpcion4.getText().toString());
        outState.putString("skip",bSkip.getText().toString());
        outState.putInt("img",provincias[orden[currentPregunta]].getImgagen());
    }

    //para recuperar el estador
    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        puntuacion=savedInstanceState.getDouble("puntuacion");
        textViewPuntuacion.setText(savedInstanceState.getString("puntuaciones"));
        textViewProgreso.setText(savedInstanceState.getString("progreso"));
        currentSkips=savedInstanceState.getInt("pistas");
        bOpcion1.setText(savedInstanceState.getString("b1"));
        bOpcion2.setText(savedInstanceState.getString("b2"));
        bOpcion3.setText(savedInstanceState.getString("b3"));
        bOpcion4.setText(savedInstanceState.getString("b4"));
        bSkip.setText(savedInstanceState.getString("skip"));
       imageViewPregunta.setImageResource(savedInstanceState.getInt("img"));
    }


}
